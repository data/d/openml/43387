# OpenML dataset: Oranges-vs.-Grapefruit

https://www.openml.org/d/43387

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Oranges vs. Grapefruit
The task of separating oranges and grapefruit is fairly obvious to a human, but even with manual observation there is still a bit of error. This dataset takes the color, weight, and diameter of an "average" orange and grapefruit and generates a larger dataset containing a wide variety of values and are "oranges" and "grapefruit".
Content
The dataset is mostly fictional. I'd love to collect real data, but for now measuring starting fruit and creating artificial samples from there seems adequate.
Inspiration
Binary classification situations are numerous, but tricky for teaching situations. I needed something to create a nice binary classification dataset and still be interesting.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43387) of an [OpenML dataset](https://www.openml.org/d/43387). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43387/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43387/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43387/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

